package japa.parser.ast.expr;

import japa.parser.ast.visitor.GenericVisitor;
import japa.parser.ast.visitor.VoidVisitor;

import java.util.List;

public class NamesExpr extends NameExpr {

	private List<NameExpr> names;

	public NamesExpr(int line, int column, int endLine, int endColumn, List<NameExpr> names) {
		this.names = names;
	}

	@Override
	public <R, A> R accept(GenericVisitor<R, A> v, A arg) {
		return v.visit(this, arg);
	}

	@Override
	public <A> void accept(VoidVisitor<A> v, A arg) {
		v.visit(this, arg);
	}

	public List<NameExpr> getNames() {
		return names;
	}

}
