package japa.parser.ast.expr;

import japa.parser.ast.visitor.GenericVisitor;
import japa.parser.ast.visitor.VoidVisitor;

import java.util.List;

public class MultiExpr extends Expression {

	private List<Expression> expressions;

	public MultiExpr(List<Expression> expressions) {
		this.expressions = expressions;

	}

	@Override
	public <R, A> R accept(GenericVisitor<R, A> v, A arg) {
		return v.visit(this, arg);

	}

	@Override
	public <A> void accept(VoidVisitor<A> v, A arg) {
		v.visit(this, arg);

	}

	public List<Expression> getExpressions() {
		return expressions;
	}

}
