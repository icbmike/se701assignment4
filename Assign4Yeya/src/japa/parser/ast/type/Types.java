package japa.parser.ast.type;

import japa.parser.ast.visitor.GenericVisitor;
import japa.parser.ast.visitor.VoidVisitor;

import java.util.List;

public class Types extends Type {

	private List<Type> types;

	public Types(List<Type> types){
		this.types = types;
		
	}
	
	@Override
	public <R, A> R accept(GenericVisitor<R, A> v, A arg) {
		return v.visit(this, arg);
		
	}

	@Override
	public <A> void accept(VoidVisitor<A> v, A arg) {
		v.visit(this, arg);
	}

	public List<Type> getTypes() {
		return types;
	}
	
	public String toString(){
		String s = "";
		for (Type t : types) {
			s += t.toString() + ",";
		}
		
		return s.substring(0, s.length() -1 );
	}
}
