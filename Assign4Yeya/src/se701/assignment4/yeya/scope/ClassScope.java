package se701.assignment4.yeya.scope;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import se701.assignment4.yeya.CompilationException;
import se701.assignment4.yeya.symbols.MethodSymbol;
import se701.assignment4.yeya.symbols.Symbol;
import se701.assignment4.yeya.symbols.TypeSymbol;
import se701.assignment4.yeya.symbols.VariableSymbol;

public class ClassScope extends Scope {

	private HashMap<String, VariableSymbol> _fields;
	private HashMap<MethodSymbol, MethodSymbol> _methods;
	private HashMap<String, TypeSymbol> _innerClasses;

	private List<ClassScope> _parentScopes = null;

	public ClassScope(Scope enclosingScope) {
		super(enclosingScope, "Class Scope");

		_fields = new HashMap<String, VariableSymbol>();
		_methods = new HashMap<MethodSymbol, MethodSymbol>();
		_innerClasses = new HashMap<String, TypeSymbol>();
		_parentScopes = new ArrayList<ClassScope>();
	}

	public List<ClassScope> getParentScopes() {
		return _parentScopes;

	}

	public void addParentScope(ClassScope parentScope) {
		this._parentScopes.add(parentScope);
	}

	/*
	 * Defines fields
	 */

	@Override
	public void defineVariable(VariableSymbol symbol) throws CompilationException {

		if (!_fields.containsKey(symbol.getName())) {
			_fields.put(symbol.getName(), symbol);
		} else {
			int lineNumber = _fields.get(symbol.getName()).getLineNumber();

			throw new CompilationException("Variable with that name already exists on line: " + lineNumber + "\nError on line: " + symbol.getLineNumber());

		}
	}

	@Override
	public void defineType(TypeSymbol symbol) throws CompilationException {
		if (!_innerClasses.containsKey(symbol.getFullName())) {
			_innerClasses.put(symbol.getFullName(), symbol);
		} else {
			throw new CompilationException("Symbol - " + symbol.getFullName() + " - already defined on line: ");
		}
	}

	@Override
	public TypeSymbol resolveType(String name) throws CompilationException {
		if (_innerClasses.containsKey(name)) {
			return _innerClasses.get(name);
		} else {
			return getEnclosingScope().resolveType(name);
		}
	}

	@Override
	public void defineMethod(MethodSymbol symbol) throws CompilationException {
		if (!_methods.containsKey(symbol)) {
			_methods.put(symbol, symbol);
		} else {
			throw new CompilationException("Method with that name already exists.\nName: " + symbol.getName() + " Error on line " + symbol.getLineNumber());
		}
	}

	@Override
	public Symbol resolveVariable(String name) throws CompilationException {
		if (_fields.containsKey(name)) {
			return _fields.get(name);
		} else{
			Symbol resolvedMember = resolveMember(name);
			return resolvedMember != null ? resolvedMember : super.resolveVariable(name); 
		}
	}

	@Override
	public MethodSymbol resolveMethod(MethodSymbol ms) throws CompilationException {
		return _methods.containsKey(ms) ? _methods.get(ms) : super.resolveMethod(ms);
	}

	public Symbol resolveMember(Object name) {
		if (_fields.containsKey(name)) {
			return _fields.get(name);
		} else if (_methods.containsKey(name)) {
			return _methods.get(name);
		} else if (_innerClasses.containsKey(name)) {
			return _fields.get(name);
		} else {
			Symbol resolvedMember = null;
			for (ClassScope parentScope : _parentScopes) {
				resolvedMember = parentScope.resolveMember(name);
			}
			return resolvedMember;
		}
	}

}
