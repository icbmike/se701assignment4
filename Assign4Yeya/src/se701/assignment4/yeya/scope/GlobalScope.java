package se701.assignment4.yeya.scope;

import java.util.HashMap;

import se701.assignment4.yeya.CompilationException;
import se701.assignment4.yeya.symbols.TypeSymbol;

public class GlobalScope extends Scope {

	private HashMap<String, TypeSymbol> _typeTable;

	public GlobalScope() {
		super(null, "Global Scope");
		_typeTable = new HashMap<String, TypeSymbol>();
	}

	@Override
	public void defineType(TypeSymbol symbol) throws CompilationException {
		if (!_typeTable.containsKey(symbol.getFullName())) {
			_typeTable.put(symbol.getFullName(), symbol);
		} else {
			throw new CompilationException("Symbol - " + symbol.getFullName() + " - already defined on line: ");
		}
	}

	@Override
	public TypeSymbol resolveType(String name) throws CompilationException {
		if (_typeTable.containsKey(name)) {
			return _typeTable.get(name);
		} else {
			return null;
		}
	}

}
