package se701.assignment4.yeya.scope;

import se701.assignment4.yeya.CompilationException;
import se701.assignment4.yeya.symbols.MethodSymbol;
import se701.assignment4.yeya.symbols.Symbol;
import se701.assignment4.yeya.symbols.TypeSymbol;
import se701.assignment4.yeya.symbols.VariableSymbol;

public class Scope {

	private Scope _enclosingScope;
	private String _scopeName;

	public Scope(Scope enclosingScope, String scopeName) {
		_enclosingScope = enclosingScope;
		_scopeName = scopeName;
	}

	public Scope getEnclosingScope() {
		return _enclosingScope;
	}

	public void defineVariable(VariableSymbol symbol) throws CompilationException {
		_enclosingScope.defineVariable(symbol);
	}

	public void defineType(TypeSymbol symbol) throws CompilationException {
		_enclosingScope.defineType(symbol);
	}

	public void defineMethod(MethodSymbol symbol) throws CompilationException {
		_enclosingScope.defineMethod(symbol);
	}

	public Symbol resolveVariable(String name) throws CompilationException {
		return _enclosingScope == null ? null : _enclosingScope.resolveVariable(name);
	}

	public MethodSymbol resolveMethod(MethodSymbol ms) throws CompilationException {
		return _enclosingScope == null ? null : _enclosingScope.resolveMethod(ms);
	}

	public TypeSymbol resolveType(String name) throws CompilationException {
		return _enclosingScope.resolveType(name);
	}

	public String getScopeName() {
		return _scopeName;
	}

}
