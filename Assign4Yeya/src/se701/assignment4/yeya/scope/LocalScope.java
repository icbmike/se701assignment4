package se701.assignment4.yeya.scope;

import java.util.HashMap;

import se701.assignment4.yeya.CompilationException;
import se701.assignment4.yeya.symbols.Symbol;
import se701.assignment4.yeya.symbols.VariableSymbol;

public class LocalScope extends Scope {

	private HashMap<String, VariableSymbol> _variables;

	public LocalScope(Scope enclosingScope) {
		super(enclosingScope, "Local Scope");
		_variables = new HashMap<String, VariableSymbol>();
	}

	/*
	 * Define parameters
	 */

	@Override
	public void defineVariable(VariableSymbol symbol) throws CompilationException {
		if (!_variables.containsKey(symbol.getName())) {
			Scope enclosingScope = this;
			VariableSymbol resolvedVariable = (VariableSymbol) enclosingScope.resolveVariable(symbol.getName());
			if (resolvedVariable == null || resolvedVariable.isField()) {
				_variables.put(symbol.getName(), symbol);
			}else{
				throw new CompilationException("Variable with that name already exists on line " + resolvedVariable.getLineNumber() + "- " + symbol.getName() + " - Error on line: " + symbol.getLineNumber());
			}
		} else {
			throw new CompilationException("Variable with that name already exists - " + symbol.getName() + " - Error on line: " + symbol.getLineNumber());
		}
	}

	/*
	 * Define parameters
	 */

	@Override
	public Symbol resolveVariable(String name) throws CompilationException {
		return _variables.containsKey(name) ? _variables.get(name) : super.resolveVariable(name);
	}

}
