package se701.assignment4.yeya;

import japa.parser.JavaParser;
import japa.parser.ast.CompilationUnit;

import java.io.File;

import se701.assignment4.yeya.visitors.ASTModiferVisitor;
import se701.assignment4.yeya.visitors.DefinitionVisitor;
import se701.assignment4.yeya.visitors.InheritenceVisitor;
import se701.assignment4.yeya.visitors.MemberVisitor;
import se701.assignment4.yeya.visitors.ParserVisitor;
import se701.assignment4.yeya.visitors.ResolvingVisitor;
import se701.assignment4.yeya.visitors.ScopeVisitor;
import se701.assignment4.yeya.visitors.TypingVisitor;

public class MainRunner {

	/**
	 * @param args
	 */
	public static void main(String... args) {
		try {
//			File src = new File("Test1.javax");
			File src = new File("Mike.javax");
			
			CompilationUnit tree = JavaParser.parse(src);

			visit(tree);
			
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void visit(CompilationUnit tree){
		
		
		ScopeVisitor name = new ScopeVisitor();
		TypingVisitor typeVisitor = new TypingVisitor();
		InheritenceVisitor inheritenceVisitor = new InheritenceVisitor();
		MemberVisitor memberVisitor = new MemberVisitor();
		DefinitionVisitor definitionVisitor = new DefinitionVisitor();
		ResolvingVisitor resolvingVisitor = new ResolvingVisitor();
		//ASTModiferVisitor astModiferVisitor = new ASTModiferVisitor();
		
		tree.accept(name, null);
		tree.accept(typeVisitor, null);
		tree.accept(inheritenceVisitor, null);
		tree.accept(memberVisitor, null);
		tree.accept(definitionVisitor, null);
		tree.accept(resolvingVisitor, null);
		tr//ee.accept(astModiferVisitor, null);
	}
}

