package se701.assignment4.yeya.symbols;


public interface TypeSymbol{

	public String getName();
	public String getFullName();
	public String getPackage();
	public Symbol resolveMember(Object name);

}
