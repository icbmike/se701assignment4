package se701.assignment4.yeya.symbols;

import se701.assignment4.yeya.scope.ClassScope;

public class ClassOrInterfaceSymbol extends Symbol implements TypeSymbol {

	private ClassScope _classScope;

	public ClassOrInterfaceSymbol(String name, int modifiers, int lineNumber, ClassScope classScope) {
		super(name, null, modifiers, lineNumber);
		this._classScope = classScope;
	}

	@Override
	public String getFullName() {
		return getName();
	}

	@Override
	public String getPackage() {
		return "";
	}

	public ClassScope getClassScope() {
		return _classScope;
	}

	@Override
	public Symbol resolveMember(Object name) {
		return _classScope.resolveMember(name);
	}

}
