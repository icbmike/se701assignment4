package se701.assignment4.yeya.symbols;


public class VariableSymbol extends Symbol {

	private boolean _isField;
	
	public VariableSymbol(String name, TypeSymbol type, int modifiers, int lineNumber, boolean isField){
		super(name, type, modifiers, lineNumber);
		_isField = isField;
		
	}

	public boolean isField() {
		return _isField;
	}
	
}
