package se701.assignment4.yeya.symbols;


public class Symbol {

	private TypeSymbol _type;
	protected String _name;
	protected int _modifiers;
	private int _lineNumber;

	public Symbol(String name, TypeSymbol type, int modifiers, int lineNumber) {
		_name = name;
		_type = type;
		_modifiers = modifiers;
		this._lineNumber = lineNumber;
	}

	public String getName() {
		return _name;
	}

	public TypeSymbol getType() {
		return _type;
	}
	
	public int getModifiers() {
		return _modifiers;
	}

	public int getLineNumber() {
		return _lineNumber;
	}


}