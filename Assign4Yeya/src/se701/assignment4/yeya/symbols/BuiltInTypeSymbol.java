package se701.assignment4.yeya.symbols;

public class BuiltInTypeSymbol implements TypeSymbol {

	private String name;
	private String packageName;

	public BuiltInTypeSymbol(String name, String packageName) {
		this.name = name;
		this.packageName = packageName;

	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public String getFullName() {
		return name;
	}

	@Override
	public String getPackage() {
		return "";
	}

	@Override
	public Symbol resolveMember(Object s) {
		return null;
	}

}
