package se701.assignment4.yeya.symbols;

public class MultiTypeSymbol implements TypeSymbol {

	private String name;

	public MultiTypeSymbol(String name){
		this.name = name;
		
	}
	
	@Override
	public String getName() {
		return name;
	}

	@Override
	public String getFullName() {
		return name;
	}

	@Override
	public String getPackage() {
		return "";
	}

	@Override
	public Symbol resolveMember(Object name) {
		return null;
	}
	
	public boolean equals(Object obj){
		MultiTypeSymbol ms = (MultiTypeSymbol)obj;
		return getName().equals(ms.getName());
	}

}
