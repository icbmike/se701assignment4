package se701.assignment4.yeya.symbols;

import japa.parser.ast.body.Parameter;

import java.util.List;

import se701.assignment4.yeya.scope.Scope;

public class MethodSymbol extends Symbol {

	private Scope methodScope;
	private List<Parameter> parameters;

	public MethodSymbol(String name, TypeSymbol type, int modifiers, int lineNumber, Scope methodScope, List<Parameter> parameter) {
		super(name, type, modifiers, lineNumber);
		this.methodScope = methodScope;
		this.parameters = parameter;
	}

	public MethodSymbol(String name, Scope methodScope, List<Parameter> parameters) {
		this(name, null, 0, 0, methodScope, parameters);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getName() == null) ? 0 : getName().hashCode());
		if (parameters != null) {
			for (Parameter p : parameters) {
				result = prime * result + ((p.getType() == null) ? 0 : p.getType().hashCode());
			}
		}
		return result;
	}

	public List<Parameter> getParameters() {
		return parameters;
	}
	
	public int getNumberOfParameters(){
		return parameters == null ? 0 : parameters.size();
	}

	@Override
	public boolean equals(Object obj) {
		MethodSymbol ms = (MethodSymbol) obj;
		boolean parametersAreEqual;
		
		if (ms.getNumberOfParameters() == getNumberOfParameters()) {
			parametersAreEqual = true;
			for (int i = 0; i < getNumberOfParameters(); i++) {
				if (!parameters.get(i).getType().equals(ms.getParameters().get(i).getType())) {
					parametersAreEqual = false;
					break;
				}
			}

		} else {
			parametersAreEqual = false;
		}
		return ms.getName().equals(getName()) && parametersAreEqual;
	}

	public Scope getMethodScope() {
		return methodScope;
	}

}
