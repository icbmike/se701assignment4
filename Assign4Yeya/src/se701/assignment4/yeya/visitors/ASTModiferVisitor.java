package se701.assignment4.yeya.visitors;

import japa.parser.ast.Node;
import japa.parser.ast.body.MethodDeclaration;
import japa.parser.ast.body.VariableDeclarator;
import japa.parser.ast.body.VariableDeclaratorId;
import japa.parser.ast.expr.ArrayAccessExpr;
import japa.parser.ast.expr.ArrayCreationExpr;
import japa.parser.ast.expr.ArrayInitializerExpr;
import japa.parser.ast.expr.AssignExpr;
import japa.parser.ast.expr.AssignExpr.Operator;
import japa.parser.ast.expr.Expression;
import japa.parser.ast.expr.IntegerLiteralExpr;
import japa.parser.ast.expr.MethodCallExpr;
import japa.parser.ast.expr.MultiExpr;
import japa.parser.ast.expr.NameExpr;
import japa.parser.ast.expr.NamesExpr;
import japa.parser.ast.expr.VariableDeclarationExpr;
import japa.parser.ast.stmt.BlockStmt;
import japa.parser.ast.stmt.ExpressionStmt;
import japa.parser.ast.stmt.ReturnStmt;
import japa.parser.ast.stmt.Statement;
import japa.parser.ast.type.ClassOrInterfaceType;
import japa.parser.ast.type.Types;
import japa.parser.ast.visitor.ModifierVisitorAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ASTModiferVisitor extends ModifierVisitorAdapter<Integer> {

	public static final int LEFT = 0;
	public static final int RIGHT = 1;
	public static final int RETURN = 2;
	public static final int METHODDEC = 3;

	HashMap<String, Integer> _methodNumReturnTypes;

	public ASTModiferVisitor() {
		_methodNumReturnTypes = new HashMap<String, Integer>();
	}

	@Override
	public Node visit(AssignExpr n, Integer arg) {
		/*
		 * extract object[] assign to variables
		 */
		List<Statement> stmts = new ArrayList<Statement>();

		// Visit Right;
		stmts.add((Statement) n.getValue().accept(this, RIGHT));

		// Visit left;
		stmts.add((Statement) n.getTarget().accept(this, LEFT));

		BlockStmt block = new BlockStmt(stmts);
		System.out.println(block);
		super.visit(n, arg);
		return block;
	}

	@Override
	public Node visit(MethodCallExpr n, Integer arg) {

		System.out.println(n.getName());

		if (_methodNumReturnTypes.containsKey(n.getName())) {
			System.out.println(_methodNumReturnTypes.get(n.getName()));

		}
		super.visit(n, arg);
		return n;
	}

	@Override
	public Node visit(MethodDeclaration n, Integer arg) {
		// Store num types length
		if (n.getType() instanceof Types) {
			_methodNumReturnTypes.put(n.getName(), ((Types) n.getType()).getTypes().size());
			// Change return type to Object[]
			n.getType().accept(this, METHODDEC);
		}
		super.visit(n, arg);
		return n;
	}

	@Override
	public Node visit(ReturnStmt n, Integer arg) {
		if (n.getExpr() != null) {
			Node accept = n.getExpr().accept(this, RETURN);
			return accept;
		}
		super.visit(n, arg);
		return n;
	}

	// MY SHIT

	@Override
	public Node visit(Types n, Integer arg) {
		super.visit(n, arg);
		return new ClassOrInterfaceType("Object[]");
	}

	@Override
	public Node visit(MultiExpr n, Integer arg) {
		super.visit(n, arg);
		return n;
	}

	@Override
	public Node visit(NamesExpr n, Integer arg) {
		Node newNode = n;
		if (arg != null) {
			if (arg == RIGHT || arg == RETURN) {
				// We need to turn this into an object array;
				VariableDeclarator variableDeclarator = new VariableDeclarator(new VariableDeclaratorId("$o"));
				List<VariableDeclarator> l = new ArrayList<VariableDeclarator>();
				l.add(variableDeclarator);

				List<Expression> names = new ArrayList<Expression>();
				for (NameExpr name : n.getNames()) {
					names.add(name);
				}
				ArrayCreationExpr arrayCreationExpr = new ArrayCreationExpr(new ClassOrInterfaceType("Object"), n.getNames().size() - 1, new ArrayInitializerExpr(names));
				if (arg == RIGHT) {
					newNode = new ExpressionStmt(new AssignExpr(new VariableDeclarationExpr(new ClassOrInterfaceType("Object"), l), arrayCreationExpr, Operator.assign));
				} else {
					newNode = new ExpressionStmt(arrayCreationExpr);
				}
			} else {
				// LEFT;
				List<Statement> stmts = new ArrayList<Statement>();
				int i = 0;
				for (NameExpr ne : n.getNames()) {
					ExpressionStmt expressionStmt = new ExpressionStmt(new AssignExpr(ne, new ArrayAccessExpr(new NameExpr("$o"), new IntegerLiteralExpr("" + i)), Operator.assign));
					stmts.add(expressionStmt);
					i++;
				}

				newNode = new BlockStmt(stmts);
			}
		}
		super.visit(n, arg);
		return newNode;
	}

}
