package se701.assignment4.yeya.visitors;

import japa.parser.ast.body.ClassOrInterfaceDeclaration;
import japa.parser.ast.type.ClassOrInterfaceType;
import japa.parser.ast.visitor.VoidVisitorAdapter;
import se701.assignment4.yeya.CompilationException;
import se701.assignment4.yeya.scope.ClassScope;
import se701.assignment4.yeya.symbols.ClassOrInterfaceSymbol;

public class InheritenceVisitor extends VoidVisitorAdapter<Void> {

	@Override
	public void visit(ClassOrInterfaceDeclaration n, Void arg) {

		ClassScope scope = (ClassScope) n.getScopeYeya();

		if (n.getExtends() != null) {
			for (ClassOrInterfaceType extend : n.getExtends()) {
				ClassOrInterfaceSymbol parentSymbol = (ClassOrInterfaceSymbol) scope.resolveType(extend.toString());
				if (parentSymbol == null) {
					throw new CompilationException("Unknown type extending: " + extend);
				} else {
					scope.addParentScope(parentSymbol.getClassScope());
				}
			}
		}

		if (n.getImplements() != null) {
			for (ClassOrInterfaceType implement : n.getImplements()) {
				ClassOrInterfaceSymbol parentSymbol = (ClassOrInterfaceSymbol) scope.resolveType(implement.toString());
				if (parentSymbol == null) {
					throw new CompilationException("Unknown type implementing: " + implement);
				} else {
					scope.addParentScope(parentSymbol.getClassScope());
				}
			}
		}

		super.visit(n, arg);
	}
}
