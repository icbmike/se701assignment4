package se701.assignment4.yeya.visitors;

import japa.parser.ast.body.FieldDeclaration;
import japa.parser.ast.body.MethodDeclaration;
import japa.parser.ast.body.VariableDeclarator;
import japa.parser.ast.expr.AssignExpr;
import japa.parser.ast.expr.BooleanLiteralExpr;
import japa.parser.ast.expr.CharLiteralExpr;
import japa.parser.ast.expr.DoubleLiteralExpr;
import japa.parser.ast.expr.Expression;
import japa.parser.ast.expr.FieldAccessExpr;
import japa.parser.ast.expr.IntegerLiteralExpr;
import japa.parser.ast.expr.LongLiteralExpr;
import japa.parser.ast.expr.MethodCallExpr;
import japa.parser.ast.expr.NameExpr;
import japa.parser.ast.expr.NamesExpr;
import japa.parser.ast.expr.NullLiteralExpr;
import japa.parser.ast.expr.StringLiteralExpr;
import japa.parser.ast.stmt.ReturnStmt;
import japa.parser.ast.visitor.GenericVisitorAdapter;
import se701.assignment4.yeya.CompilationException;
import se701.assignment4.yeya.symbols.MethodSymbol;
import se701.assignment4.yeya.symbols.MultiTypeSymbol;
import se701.assignment4.yeya.symbols.Symbol;
import se701.assignment4.yeya.symbols.TypeSymbol;

public class ResolvingVisitor extends GenericVisitorAdapter<TypeSymbol, Void> {

	private TypeSymbol _currentMethodReturnType;

	@Override
	public TypeSymbol visit(AssignExpr n, Void arg) {
		TypeSymbol targetType = n.getTarget().accept(this, arg);

		TypeSymbol valueType = n.getValue().accept(this, arg);

		if (!targetType.equals(valueType)) {
			throw new CompilationException("Incompatible types on line: " + n.getBeginLine());
		}

		return super.visit(n, arg);
	}

	@Override
	public TypeSymbol visit(BooleanLiteralExpr n, Void arg) {
		return n.getScopeYeya().resolveType("boolean");
	}

	@Override
	public TypeSymbol visit(FieldDeclaration n, Void arg) {
		TypeSymbol type = n.getScopeYeya().resolveType(n.getType().toString());

		for (VariableDeclarator v : n.getVariables()) {
			Expression init = v.getInit();
			if (init != null) {
				if (!type.equals(init.accept(this, arg))) {
					throw new CompilationException("Incompatible types on line: " + v.getBeginLine());
				}
			}
		}
		return type;
	}

	@Override
	public TypeSymbol visit(FieldAccessExpr n, Void arg) {
		Expression callScope = n.getScope();
		// Get the parameters

		Symbol resolvedVariable = n.getScopeYeya().resolveVariable(callScope.toString());
		Symbol resolvedMember;
		if (resolvedVariable == null) {
			throw new CompilationException("Variable " + callScope + " is not defined in this scope: " + n.getBeginLine());
		} else {
			resolvedMember = resolvedVariable.getType().resolveMember(n.getField());
			if (resolvedMember == null) {
				throw new CompilationException("Cant resolve field: " + n.getField() + " on line: " + n.getBeginLine());
			}
		}
		return resolvedMember.getType();

	}

	@Override
	public TypeSymbol visit(CharLiteralExpr n, Void arg) {
		return n.getScopeYeya().resolveType("char");
	}

	@Override
	public TypeSymbol visit(DoubleLiteralExpr n, Void arg) {
		return n.getScopeYeya().resolveType("double");
	}

	@Override
	public TypeSymbol visit(IntegerLiteralExpr n, Void arg) {
		return n.getScopeYeya().resolveType("int");
	}

	@Override
	public TypeSymbol visit(LongLiteralExpr n, Void arg) {
		return n.getScopeYeya().resolveType("long");
	}

	@Override
	public TypeSymbol visit(NullLiteralExpr n, Void arg) {
		return n.getScopeYeya().resolveType("Object");
	}

	@Override
	public TypeSymbol visit(StringLiteralExpr n, Void arg) {
		return n.getScopeYeya().resolveType("String");
	}

	@Override
	public TypeSymbol visit(NameExpr n, Void arg) {
		Symbol resolvedVariable = n.getScopeYeya().resolveVariable(n.getName());
		if (resolvedVariable == null) {
			throw new CompilationException("Variable " + n.getName() + " is not defined in this scope : " + n.getBeginLine());
		} else if (resolvedVariable.getLineNumber() > n.getBeginLine()) {
			throw new CompilationException("Variable " + n.getName() + " is used before it is defined : " + n.getBeginLine());
		}

		return resolvedVariable.getType();
	}

	@Override
	public TypeSymbol visit(MethodCallExpr n, Void arg) {
		Expression callScope = n.getScope();
		// Get the parameters

		// Construct a method symbol
		MethodSymbol methodSymbol = new MethodSymbol(n.getName(), n.getScopeYeya(), null);

		Symbol resolvedMethod;
		if (callScope == null) {

			resolvedMethod = n.getScopeYeya().resolveMethod(methodSymbol);
			if (resolvedMethod == null) {
				throw new CompilationException("No such method " + n.getName() + "() on line " + n.getBeginLine());
			}
		} else {
			Symbol resolvedVariable = n.getScopeYeya().resolveVariable(callScope.toString());
			if (resolvedVariable == null) {
				throw new CompilationException("Variable " + callScope + " is not defined in this scope: " + n.getBeginLine());
			} else {
				resolvedMethod = resolvedVariable.getType().resolveMember(methodSymbol);
				if (resolvedMethod == null) {
					throw new CompilationException("Cant resolve method: " + methodSymbol.getName() + "on line: " + n.getBeginLine());
				}
			}
		}
		return resolvedMethod.getType();
	}

	@Override
	public TypeSymbol visit(ReturnStmt n, Void arg) {

		if (!_currentMethodReturnType.equals(n.getExpr().accept(this, arg))) {
			throw new CompilationException("Return type doesn't match method type on line: " + n.getBeginLine());
		}
		return super.visit(n, arg);
	}

	@Override
	public TypeSymbol visit(MethodDeclaration n, Void arg) {
		// Construct a method symbol
		_currentMethodReturnType = n.getScopeYeya().resolveType(n.getType().toString());
		return super.visit(n, arg);
	}

	@Override
	public TypeSymbol visit(NamesExpr n, Void arg) {
		String typeName = "";
		for (NameExpr ne : n.getNames()) {
			typeName += ne.accept(this, arg).getName() + ",";
		}
		typeName = typeName.substring(0, typeName.length() - 1);
		MultiTypeSymbol multiTypeSymbol = new MultiTypeSymbol(typeName);
		return multiTypeSymbol;
	}

}
