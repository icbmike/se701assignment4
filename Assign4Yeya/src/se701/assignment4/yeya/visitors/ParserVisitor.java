package se701.assignment4.yeya.visitors;

import japa.parser.ast.expr.Expression;
import japa.parser.ast.expr.MultiExpr;
import japa.parser.ast.expr.NameExpr;
import japa.parser.ast.expr.NamesExpr;
import japa.parser.ast.type.Type;
import japa.parser.ast.type.Types;
import japa.parser.ast.visitor.VoidVisitorAdapter;

public class ParserVisitor extends VoidVisitorAdapter<Void> {

	@Override
	public void visit(Types n, Void arg) {
		for (Type t : n.getTypes()) {
			// System.out.println(t);
		}
		super.visit(n, arg);
	}

	@Override
	public void visit(MultiExpr n, Void arg) {
		for (Expression e : n.getExpressions()) {
			System.out.println(e);
		}
		super.visit(n, arg);
	}
	
	

//	@Override
//	public void visit(NameExpr n, Void arg) {
//		System.out.println(n);
//		super.visit(n, arg);
//	}
	
	@Override
	public void visit(NamesExpr n, Void arg) {
		for(NameExpr na : n.getNames()){
			System.out.println(na);
		}
		super.visit(n, arg);
	}

}
