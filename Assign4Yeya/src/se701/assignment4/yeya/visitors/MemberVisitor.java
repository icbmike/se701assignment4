package se701.assignment4.yeya.visitors;

import japa.parser.ast.body.ClassOrInterfaceDeclaration;
import japa.parser.ast.body.ConstructorDeclaration;
import japa.parser.ast.body.EnumDeclaration;
import japa.parser.ast.body.FieldDeclaration;
import japa.parser.ast.body.MethodDeclaration;
import japa.parser.ast.body.Parameter;
import japa.parser.ast.body.VariableDeclarator;
import japa.parser.ast.type.Type;
import japa.parser.ast.type.Types;
import japa.parser.ast.visitor.VoidVisitorAdapter;
import se701.assignment4.yeya.CompilationException;
import se701.assignment4.yeya.scope.Scope;
import se701.assignment4.yeya.symbols.ClassOrInterfaceSymbol;
import se701.assignment4.yeya.symbols.MethodSymbol;
import se701.assignment4.yeya.symbols.MultiTypeSymbol;
import se701.assignment4.yeya.symbols.TypeSymbol;
import se701.assignment4.yeya.symbols.VariableSymbol;

public class MemberVisitor extends VoidVisitorAdapter<Void> {
	
	private ClassOrInterfaceSymbol _currentClass;
	
	@Override
	public void visit(ClassOrInterfaceDeclaration n, Void arg) {
		_currentClass = (ClassOrInterfaceSymbol) n.getSymbol();
		super.visit(n, arg);
	}

	@Override
	public void visit(EnumDeclaration n, Void arg) {
		_currentClass = (ClassOrInterfaceSymbol) n.getSymbol();
		super.visit(n, arg);
	}

	@Override
	public void visit(FieldDeclaration n, Void args) {

		Scope scope = n.getScopeYeya();

		for (VariableDeclarator v : n.getVariables()) {
			
			TypeSymbol type = scope.resolveType(n.getType().toString());
			if(type == null){
				throw new CompilationException("Type doesnt exist: " + n.getType() + "\ton line: " + n.getBeginLine());
			}
			VariableSymbol variableSymbol = new VariableSymbol(v.getId().toString(), type , n.getModifiers(), v.getBeginLine(), true);
			scope.defineVariable(variableSymbol);
			v.setSymbol(variableSymbol);
		}

		super.visit(n, args);
	}

	@Override
	public void visit(MethodDeclaration n, Void args) {
		Scope methodScope = n.getScopeYeya();
		
		if(n.getType() instanceof Types){
			Types ts = (Types) n.getType();
			for(Type t : ts.getTypes()){
				TypeSymbol type = methodScope.resolveType(t.toString());
				if(type == null){
					throw new CompilationException("Type doesnt exist: " + t);
				}
			}
			methodScope.getEnclosingScope().defineType(new MultiTypeSymbol(n.getType().toString()));
			
		}
		
		TypeSymbol type = methodScope.resolveType(n.getType().toString());
		if(type == null){
			throw new CompilationException("Type doesnt exist: " + n.getType());
		}
		
		MethodSymbol methodSymbol = new MethodSymbol(n.getName(), type, n.getModifiers(), n.getBeginLine(), methodScope, n.getParameters());
		methodScope.defineMethod(methodSymbol);
		n.setSymbol(methodSymbol);

		super.visit(n, args);
	}

	@Override
	public void visit(ConstructorDeclaration n, Void args) {
		Scope methodScope = n.getScopeYeya();
		MethodSymbol methodSymbol = new MethodSymbol(n.getName(), null, n.getModifiers(), n.getBeginLine(), methodScope, n.getParameters());
		methodScope.defineMethod(methodSymbol);
		n.setSymbol(methodSymbol);

		super.visit(n, args);
	}

	@Override
	public void visit(Parameter n, Void args) {
		
		Scope scope = n.getScopeYeya();
		
		TypeSymbol type = scope.resolveType(n.getType().toString());
		if(type == null){
			throw new CompilationException("Type doesnt exist: " + n.getType());
		}
		
		VariableSymbol variableSymbol = new VariableSymbol(n.getId().toString(), type, n.getModifiers(), n.getBeginLine(), false);
		scope.defineVariable(variableSymbol);

		n.setSymbol(variableSymbol);

		super.visit(n, args);
	}

}
