package se701.assignment4.yeya.visitors;

import se701.assignment4.yeya.CompilationException;
import se701.assignment4.yeya.scope.Scope;
import se701.assignment4.yeya.symbols.TypeSymbol;
import se701.assignment4.yeya.symbols.VariableSymbol;
import japa.parser.ast.body.VariableDeclarator;
import japa.parser.ast.expr.VariableDeclarationExpr;
import japa.parser.ast.visitor.VoidVisitorAdapter;

public class DefinitionVisitor extends VoidVisitorAdapter<Void> {

	@Override
	public void visit(VariableDeclarationExpr n, Void arg) {
		Scope scope = n.getScopeYeya();

		TypeSymbol type = scope.resolveType(n.getType().toString());
		if (type == null) {
			throw new CompilationException("Unknown type: " + n.getType() + " on line: " + n.getBeginLine());
		}

		for (VariableDeclarator v : n.getVars()) {
			VariableSymbol variableSymbol = new VariableSymbol(v.getId().toString(), type, n.getModifiers(), v.getBeginLine(), false);
			scope.defineVariable(variableSymbol);
		}

		super.visit(n, arg);
	}

}
