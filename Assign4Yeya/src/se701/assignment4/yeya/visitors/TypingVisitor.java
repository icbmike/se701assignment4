package se701.assignment4.yeya.visitors;

import japa.parser.ast.CompilationUnit;
import japa.parser.ast.ImportDeclaration;
import japa.parser.ast.PackageDeclaration;
import japa.parser.ast.body.ClassOrInterfaceDeclaration;
import japa.parser.ast.body.EnumDeclaration;
import japa.parser.ast.visitor.VoidVisitorAdapter;
import se701.assignment4.yeya.scope.ClassScope;
import se701.assignment4.yeya.scope.GlobalScope;
import se701.assignment4.yeya.symbols.BuiltInTypeSymbol;
import se701.assignment4.yeya.symbols.ClassOrInterfaceSymbol;

public class TypingVisitor extends VoidVisitorAdapter<Void> {

	@Override
	public void visit(ClassOrInterfaceDeclaration n, Void arg) {

		ClassScope classScope = (ClassScope) n.getScopeYeya();
		ClassOrInterfaceSymbol classSymbol = new ClassOrInterfaceSymbol(n.getName(), n.getModifiers(), n.getBeginLine(), classScope);

		// Define the symbol and type on the enclosing scope
		classScope.getEnclosingScope().defineType(classSymbol);
		n.setSymbol(classSymbol);

		super.visit(n, arg);

	}

	@Override
	public void visit(CompilationUnit n, Void arg) {

		String[] builtInTypes = new String[] { "int", "boolean", "float", "double", "long", "String", "void", "char", "Object"};

		GlobalScope globalScope = (GlobalScope) n.getScopeYeya();
		for (final String builtinType : builtInTypes) {
			globalScope.defineType(new BuiltInTypeSymbol(builtinType, ""));
		}

		super.visit(n, arg);
	}

	@Override
	public void visit(EnumDeclaration n, Void arg) {
		ClassScope classScope = (ClassScope) n.getScopeYeya();
		ClassOrInterfaceSymbol classSymbol = new ClassOrInterfaceSymbol(n.getName(), n.getModifiers(), n.getBeginLine(), classScope);

		// Define the symbol and type on the enclosing scope

		classScope.getEnclosingScope().defineType(classSymbol);
		n.setSymbol(classSymbol);

		super.visit(n, arg);

	}

	@Override
	public void visit(ImportDeclaration n, Void arg) {
		GlobalScope globalScope = (GlobalScope) n.getScopeYeya();
		int lastIndexOf = n.getName().toString().lastIndexOf(".");
		String packageName = n.getName().toString().substring(0, lastIndexOf);

		BuiltInTypeSymbol builtInTypeSymbol = new BuiltInTypeSymbol(n.getName().getName(), packageName);
		globalScope.defineType(builtInTypeSymbol);

		super.visit(n, arg);

	}

	@Override
	public void visit(PackageDeclaration n, Void arg) {
		super.visit(n, arg);

	}

}
